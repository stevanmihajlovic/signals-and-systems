clc;
funcprot(0);

///////////////
//PROBLEM 1.1//
///////////////

printf("\nProblem 1.1:\n");

// a) 1/2 e^jπ
dekart=0.5*(cos(%pi)+%i*sin(%pi));
disp(dekart);

// b) 1/2 e^(-jπ)
dekart=0.5*(cos(%pi)-%i*sin(%pi));
disp(dekart);

// c) e^(j π/2)
dekart=cos(%pi*0.5)+%i*sin(%pi*0.5);
disp(dekart);

// d) e^(-j π/2)
dekart=cos(%pi*0.5)-%i*sin(%pi*0.5);
disp(dekart);

// e) e^(5j π/2)
dekart=cos(5*%pi/2)+%i*sin(5*%pi/2);
disp(dekart);

// f) √2 e^(j π/4)
dekart=sqrt(2)*(cos(%pi/4)+%i*sin(%pi/4));
disp(dekart);

// g) √2 e^(j 9π/4)
dekart=sqrt(2)*(cos(9*%pi/4)+%i*sin(9*%pi/4));
disp(dekart);

// h) √2 e^(-j 9π/4)
dekart=sqrt(2)*(cos(9*%pi/4)-%i*sin(9*%pi/4));
disp(dekart);

// i) √2 e^(-j π/4)
dekart=sqrt(2)*(cos(%pi/4)-%i*sin(%pi/4));
disp(dekart);


///////////////
//PROBLEM 1.2//
///////////////

printf("\nProblem 1.2:\n");

// a) 5
dekart=5;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// b)-2
dekart=-2;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// c)-3j
dekart=-3*%i;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// d)1/2-j √3/2
dekart=0.5-%i*sqrt(3)/2;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// e) 1+j
dekart=1+%i;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// f) (1-j)^2
dekart=(1-%i)^2;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// g) j(1-j)
dekart=(1-%i)*%i;
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// h) (1+j)/(1-j)
dekart=(1+%i)/(1-%i);
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);

// i) (√2+j√2)/(1+j√3)
dekart=(sqrt(2)+%i*sqrt(2))/(1+%i*sqrt(3));
mod=sqrt(real(dekart)^2+imag(dekart)^2);
if (real(dekart)==0) then
    if (imag(dekart)>0) then
        fi=%pi/2;
    else
        fi=-%pi/2;
    end
else
    if (real(dekart) > 0) then
        fi=atan(imag(dekart)/real(dekart));
    else
        if (imag(dekart) > 0) then
           fi=atan(imag(dekart)/real(dekart))+%pi;
        else
           fi=atan(imag(dekart)/real(dekart))-%pi;
        end
    end
end
printf("Broj u polarnom obliku je: %.2f*e^j%.2f\n",mod,fi);


///////////////
//PROBLEM 1.3//
///////////////

printf("\nProblem 1.3:\n");

// a)
function y=f(x)
    y = exp(-4*x);
endfunction

E = intg(0, 8314, f); //8314 predstavlja maksimalni raspon (u slučaju Scilab-a)
if (E < %inf) then
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);

// b)
// Uzete su granice -4157 i 4157 za -/+ beskonacnost u Scilab-u
function y=f(x)
    y = abs(exp(%i*(2*x+%pi/4)))^2;
endfunction

E = intg(-4157, 4157, f); 
if (E >= 4157) then //ako predje granicu beskonacnosti mozemo izracunati snagu
    
    E = %inf;
    // P = lim(T)->∞ {1/2T*[∫dt (od -T do +T)]} => P = 1
    P = 1;    
else
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);

// c)
function y=f(x)
    y = cos(x)^2;
endfunction

E = intg(-1750, 1750 , f); //zbog ogranicenja scilaba za beskonacnosti
if (E>=1750) then //ako predje ovu vrednost onda je E=beskonacno moze se izracunati snaga
    E = %inf;
    //cos(x)^2=(1+cos2x)/2
    // P = lim(T)->∞ [1/(2*T)*∫(1+cos(2t)/2) dt (od -T do +T)] => P = 1/2
    P = 1/2;
else
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);

// d)
E = 1/(1-1/4);
if E < 20000 then
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);

// e)
function y=f(x)
    y = abs(exp(%i*(%pi/2*x+%pi/8)))^2;
endfunction
E = intg(-4157, 4157 , f); //granice beskonacnosti
if (E >= 4157) then // ako je energija beskonacna racunamo i snagu
    E = %inf;
    //|e^bilosta|=1 pa je
    // P = lim(N->∞) [1/(2*N+1)*sum[-N do +N])|x[n]|^2] => P=1;
    P = 1;
else
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);


// f)

function y=f(x)
    y =abs(cos(%pi/4*x))^2;
endfunction

E = intg(-1750, 1750 , f); //granice beskonacnosti
if (E>=1750) then // ako je energija beskonacna mozemo izracunati i snagu
    E = %inf;
    //cos(pi/4*x)^2=(1+cos(pi/2*x))/2
    // P = lim(N->∞) [1/(2*N+1)*sum[-N do +N](1+cos(pi/2*x))/2 => P=1/2
    P = 1/2;
else
    P = 0;
end
printf("E je %.2f, a P je %.2f\n", E, P);


///////////////
//PROBLEM 1.4//
///////////////

printf("\nProblem 1.4:\n");

//a) x[n - 3]
pomeraj=-3;
novaDonjaGranica=-2-pomeraj;
novaGornjaGranica=4-pomeraj;
printf("Signal je sigurno nula za n < %d i n > %d\n",novaDonjaGranica,novaGornjaGranica);

//b) x[n + 4]
pomeraj=4;
novaDonjaGranica=-2-pomeraj;
novaGornjaGranica=4-pomeraj;
printf("Signal je sigurno nula za n < %d i n > %d\n",novaDonjaGranica,novaGornjaGranica);

//c) x[- n]
pomeraj=0;
novaDonjaGranica=pomeraj+2;
novaGornjaGranica=pomeraj-4;
printf("Signal je okrenut i sigurno je nula za n < %d i n > %d\n",novaGornjaGranica,novaDonjaGranica);

//d) x[- n + 2]
pomeraj=2;
novaDonjaGranica=pomeraj+2;
novaGornjaGranica=pomeraj-4;
printf("Signal je okrenut i sigurno je nula za n < %d i n > %d\n",novaGornjaGranica,novaDonjaGranica);

//e) x[- n - 2]
pomeraj=-2;
novaDonjaGranica=pomeraj+2;
novaGornjaGranica=pomeraj-4;
printf("Signal je okrenut i sigurno je nula za n < %d i n > %d\n",novaGornjaGranica,novaDonjaGranica);


///////////////
//PROBLEM 1.5//
///////////////

printf("\nProblem 1.5:\n");

// a)
granica = 3;//t<3
pomeraj = 1;//1-t
novaGranica = pomeraj - granica;
printf("Sigurno je nula za t > %d\n", novaGranica);

// b)
granica = 3;
pomeraj1 = 1;
pomeraj2 = 2;
novaGranica1 = pomeraj1 - granica;
novaGranica2 = pomeraj2 - granica;
printf("Sigurno je nula za t > %d\n", max(novaGranica1, novaGranica2)); // max zbog zbira f-ja

// c)
granica = 3;
pomeraj1 = 1;
pomeraj2 = 2;
novaGranica1 = pomeraj1 - granica;
novaGranica2 = pomeraj2 - granica;
printf("Sigurno je nula za t > %d\n", min(novaGranica1, novaGranica2)); // min zbog proizvoda f-ja

// d)
pomeraj = 3;
granica = 3;
novaGranica = granica / pomeraj;
printf("Sigurno je nula za t < %d\n", novaGranica);

// e)

pomeraj = 3;
granica = 3;
novaGranica = granica * pomeraj;
printf("Sigurno je nula za t < %d\n", novaGranica);


///////////////
//PROBLEM 1.6//
///////////////

printf("\nProblem 1.6:\n");

// a)
signal=2*exp(%i*(5000+%pi/4));// u(t) = 0, za t < 0 (t je 5000 za kontrolni)
negativno=1;// jedan je u pozitivnom delu
kontrolniUzorak=signal*negativno;
periodican=0;// u pocetku nije periodican
for n = -5000:4999//aproksimacija beskonacnosti
    signal=2*exp(%i*(n+%pi/4));
    if (n<0) 
        negativno=0;
    else
        negativno=1;
    end
    trenutnaVrednost=signal*negativno;
    if (trenutnaVrednost==kontrolniUzorak) then //ako nadjemo bar jedno poklapanje periodican je
        periodican=1;
    break;
    end
end
if(periodican==1) then
    printf("Signal je periodican.\n");
else
    printf("Signal nije periodican.\n");
end

// b)
uOdN=0;
uOdNegativnogN=1;
kontrolniUzorak=uOdN+uOdNegativnogN;//za n < 0 -> u[n] = 0, u[-n] = 1
periodican=0;                       //za n > 0 -> u[n] = 1, u[-n] = 0
for n=-4999:5000
    if (n<0) then
        uOdN=0;
        uOdNegativnogN=1;
    else
        uOdN=1;
        uOdNegativnogN=0;
    end
    trenutnaVrednost=uOdN+uOdNegativnogN
    if (trenutnaVrednost==kontrolniUzorak) then
        periodican=1;
        break;
    end
end
if(periodican==1) then
    printf("Signal je periodican.\n");
else
    printf("Signal nije periodican.\n");
end

// c)
beskonacno=5000;
n=-5000;
kontrolniUzorak=0;
for k=-5000:5000// od -beskonacno do + beskonacno (aproksimacija)
    if (n-4*k==0) then
        prviDirakov=beskonacno;
    else
        prviDirakov=0;
    end
    if (n-1-4*k==0) then
        drugiDirakov=beskonacno;
    else
        drugiDirakov=0;
    end    
    kontrolniUzorak=kontrolniUzorak+(prviDirakov-drugiDirakov);
end
periodican=0;// true
for n=-4999:5000
    trenutnaVrednost=0;
    for k=-5000:5000// od -beskonacno do + beskonacno (aproksimacija)
        if (n-4*k==0) then
            prviDirakov=beskonacno;
        else
            prviDirakov=0;
        end
        if (n-1-4*k==0) then
            drugiDirakov=beskonacno;
        else
            drugiDirakov=0;
        end
        trenutnaVrednost=trenutnaVrednost+(prviDirakov-drugiDirakov);
    end
    if (trenutnaVrednost==kontrolniUzorak) then
        periodican=1;
        break;
    end
end

if(periodican==1) then
    printf("Signal je periodican.\n");
else
    printf("Signal nije periodican.\n");
end


///////////////
//PROBLEM 1.7//
///////////////

printf("\nProblem 1.7:\n");

// a)
function d=signal(n)
    if (n < 0) then
        prviSegment = 0;
    else
        prviSegment = 1;
    end
    
    if (n - 4 < 0) then
        drugiSegment = 0;
    else
        drugiSegment = 1;
    end
    
    d = prviSegment - drugiSegment;
endfunction

for k = -500:500
    paranDeo = 1/2*(signal(k) + signal(-k));
    if (paranDeo == 0) then
        printf("%d ",k);
    end
end
printf("\n");


// b)
function d=signal(n) 
    
    d = sin(0.5*n);
endfunction

for k = -500:500
    paranDeo = 1/2*(signal(k) + signal(-k));
    if (paranDeo == 0) then
        printf("%d ",k);
    end
end
printf("\n");

// c)
function d=signal(n)
    prviSegment = 0.5^n
    
    if (n - 3 < 0) then
        drugiSegment = 0;
    else
        drugiSegment = 1;
    end
    
    d = prviSegment * drugiSegment;
endfunction

for k = -500:500
    paranDeo = 1/2*(signal(k) + signal(-k));
    if (paranDeo == 0) then
        printf("%d ",k);
    end
end
printf(" i kada n -> inf.\n"); // lim n->inf od (1/2)^n=0

// d)
function d=signal(n)
    prviSegment = %e^(-5*n)
    
    if (n + 2 < 0) then
        drugiSegment = 0;
    else
        drugiSegment = 1;
    end
    
    d = prviSegment * drugiSegment;
endfunction

for k = -1000:1000
    paranDeo = 1/2*(signal(k) + signal(-k));
    if (paranDeo == 0) then
        printf("%d ",k);
    end
end
printf("Samo kada t->inf.\n");//lim t->inf od e^(-5t)=0


///////////////
//PROBLEM 1.8//
///////////////

printf("\nProblem 1.8:\n");

// a)
// trazi se da realan deo signala predstavimo u obliku A*e^(-a*t)*cos(w*t+fi)
// A,a,w su realni brojevi a -pi<fi<pi, zadatak je odrediti sve koeficijente
// x1(t)=-2=>Re{x1(t)}=2*e^(0*t)*cos(0*t+0)
x1=-2;
A = 2;
fi = 0;
a = 0;
w = 0;
printf("Re{x(t)}=%.2f*e^(-%.2f*t)*cos(%.2f*t+%.2f)\n",A, a, w, fi);

// b)
// x2(t)=√2*e^(j*pi/4)*cos(3*t+2*pi)=√2(cos(pi/4))+j*sin(pi/4))*cos(3*t+2*pi)=
// =√2*√2/2*cos(3*t+2*pi)+√2*√2/2*j*sin(3*t+2*pi)=>Re{x2(t)}=cos(3*t)=1*e^(-0*t)*cos(3*t+0)
t=1;
x2 = sqrt(2)*exp(%i*%pi/4)*cos(3*t+2*%pi);
A = 1;
fi = 0;
w = 3;
a = 0;

printf("Re{x(t)}=%.2f*e^(-%.2f*t)*cos(%.2f*t+%.2f)\n",A, a, w, fi);

// c)
// x3(t)=e^(-t)*sin(3*t+pi)=>Re{x3(t)}=1*e^(-1*t)*cos(3*t-pi/2)
t=1;
x3 = exp(-t)*sin(3*t+%pi);
A = 1 ;
fi = -%pi/2;
a = 1;
w = 3;

printf("Re{x(t)}=%.2f*e^(-%.2f*t)*cos(%.2f*t+%.2f)\n",A, a, w, fi);

// d)
// x4(t)=j*e^(-2+100*j)*t=j*e^(-2*t)*e(100*j*t)=j*e^(-2*t)*(cos100t+j*sin100t)=
// =j*e^(-2t)*cos100t-e^(-2*t)*sin(100t)=>Re{x4(t)}=e^(-2*t)*sin(100*t+pi)=1*e(-2*t)*cos(100*t+pi/2)
t=1;
x4 = %i*exp(-2+%i*100)*t;
A = 1;
fi = %pi/2;
a = 2;
w = 100;
printf("Re{x(t)}=%.2f*e^(-%.2f*t)*cos(%.2f*t+%.2f)\n",A, a, w, fi);



///////////////
//PROBLEM 1.9//
///////////////

printf("\nProblem 1.9:\n");

// a)
//x1(t)=j*e^(j*10*t)=(cos(pi/2)+j*sin(pi/2))*e^(j*10*t)=e^(j*pi/2)*e^(j*10*t)=e^(j*10*t+pi/2)
w = 10; 
t = 2*%pi/w; // perioda=2*pi/|w|
printf("Signal je periodican sa osnovnom periodom %.2f.\n",t);


// b)
//x2(t)=e^((-1+j)*t)=e^(-1*t)*e^(j*t)
// prvi signal je konstantan jer je a<0, kako je i drugi konstantan,
// i njihov proizvod je konstanta i nije periodican

printf("Signal nije periodican i nema osnovnu periodu.\n");

// c)
w = 7*%pi;
w = modulo(w, 2*%pi);
n = 2*%pi / w; 
provera = -1;
for m1 = 1:100
    perioda = m1*n;
    if (modulo(perioda, 1) == 0) then
        provera = m1;
        break;
    end
end
if (provera == -1) then
    disp("Ne postoji osnovna perioda, signal nije periodican\n");
else
    printf("Signal je periodican sa osnovnom periodom %.2f.\n",perioda);
end

// d)
w = 3*%pi/5;
w = modulo(w, 2*%pi);
n = 2*%pi / w; 
provera = -1;
for m1 = 1:100
    perioda = m1*n;
    if (modulo(perioda, 1) == 0) then
        provera = m1;
        break;
    end
end
if (provera == -1) then
    printf("Ne postoji osnovna perioda, signal nije periodican.\n");
else
    printf("Signal je periodican sa osnovnom periodom %.2f.\n",perioda);
end

// e)
w = 3/5;
w = modulo(w, 2*%pi);
n = 2*%pi / w; 
provera = -1;
for m1 = 1:100
    perioda = m1*n;
    if (modulo(perioda, 1) == 0) then
        provera = m1;
        break;
    end
end
if (provera == -1) then
    printf("Ne postoji osnovna perioda, signal nije periodican.\n");
else
    printf("Signal je periodican sa osnovnom periodom %.2f.\n",perioda);
end


////////////////
//PROBLEM 1.10//
////////////////

printf("\nProblem 1.10:\n");

w1 = 10;
w2 = 4;

t1 = 2*%pi / w1;
t2 = 2*%pi / w2;

NZS = min(t1,t2);

while (NZS < 100)
    if (modulo(NZS, t1) == 0 & modulo(NZS, t2) == 0) then
        break;
    else
        NZS = NZS + min(t1, t2);
    end
end

printf("Signal ima osnovnu periodu od %.2f.\n",NZS);


////////////////
//PROBLEM 1.11//
////////////////

printf("\nProblem 1.11:\n");

prvaPerioda = 1;
w1 = 4* %pi /7;
w2 = 2* %pi /5; 
n1 = (2* %pi)/ w1 ; 
n2 = (2* %pi)/ w2 ; 
for m1 = 1:100
    perioda = n1*m1;
    if( modulo (perioda ,1) ==0)
    drugaPerioda = perioda;
    break ;
end
end
for m2 = 1:100
    perioda = n2*m2;
    if( modulo ( perioda ,1) ==0)
    trecaPerioda = perioda;
    break ;
    end
end

V = [prvaPerioda drugaPerioda trecaPerioda];
printf("Osnovna perioda ovog signala je %d.\n",lcm(V));


////////////////
//PROBLEM 1.12//
////////////////

printf("\nProblem 1.12:\n");

function y = suma(n)
    y = 0;
    for k=3:1000
        if ((n - (1 + k)) == 0) then
            y = 1;
            break;
        end
    end
endfunction

n = -6:8;
y = [];
for t = 1:length(n)
    y(t) = 1 - suma(n(t));
end

plot2d(n,y, style=-6);
ose = get("current_axes");
ose.data_bounds = [-5,-0.5;8,1.5];

printf ("Sa grafika mozemo uociti da signal x[n] možemo dobiti inverzijom signala u[n] i kasnjenjem za 3.\n Odavde sledi da je: x[n] = u[-n+3]=>M = -1, n0 = -3.\n");


////////////////
//PROBLEM 1.13//
////////////////

printf("\nProblem 1.13:\n");

//x(t) = δ(t+2) - δ(t-2)
//y(t) = ∫x(τ)dτ [u granicama od -∞ do t]
//y(t) = ∫(δ(τ+2) - δ(τ-2))dτ [u granicama od -∞ do t]
//y(t) ima vrednost 0 za t < -2, vrednost 1 za t∈(-2,2) i vrednost 0 za t > 2.
//E = ∫|y(τ)|^2 dτ [u granicama od -∞ do +∞), odatle sledi
//E = ∫dτ [u granicama od -2 do +2]

function y = f(x)
    if (x>=0) then 
        y=1;
    else
        y=0;
    end
endfunction
function y = z(x)
    y = (f(x+2)-f(x-2))^2;
endfunction
E = intg(-2,2,z);
printf("Energija ovog signala je: %d.\n", E);


////////////////
//PROBLEM 1.14//
////////////////

printf("\nProblem 1.14:\n");

//iscrtavanje funkcije

clf;
clear;
t1=-2:0.01:-1.01;
t2=-1:0.01:0;
t3=0.01:0.01:1;
t4=1.01:0.01:2;
A=repmat([1],1,100);
B=repmat([-2],1,100);
t = [t1 t2 t3 t4];
xt = [A B A B];
xt(401)=1;
plot2d(t,xt,style = 9);

gt=diff(xt);
t11=-1.99:0.01:-1.01;
t = [t11 t2 t3 t4];
plot2d(t,gt,style=35);

xgrid(40);
a = gca();
grafikIzvoda = a.children(1).children;
grafikFje = a.children(2).children;

grafikIzvoda.foreground = 20;
grafikIzvoda.thickness = 3;
grafikFje.thickness = 3;
grafikFje.line_style = 15;

axes = get("current_axes");
axes.data_bounds = [-2,-4;2,4];
handle = legend(['x(t)';'dx(t)/dt']);

printf ("Sa grafika mozemo uociti da izvod f-je x(t) odgovara vrednosti: 3g(t)−3g(t−1).\nStoga sledi da su trazene vrednosti: A1 = 3, t1 = 0, A2 = -3 i t2 = 1\n")


////////////////
//PROBLEM 1.15//
////////////////

printf("\nProblem 1.15:\n");

// a) y2[n]=x2[n-2]+1/2*x2[n-3]=
//   y2[n]=y1[n-2]+1/2*y1[n-3]=
//   y2[n]=2*x1[n-2]+4*x1[n-3]+1/2*(2*x1[n-3]+4*x1[n-4])
//   y2[n]=2*x1[n-2]+5*x1[n-3]+2*x1[n-4]
printf ("Ulazno-izlazna karakteristika sistema je y2[n]=2*x1[n-2]+5*x1[n-3]+2*x1[n-4].\n");

// b)
x = [1 3 5 7 9 -1 10];

function y = y1(x)
    for n = 2:length(x)
        y(n) = 2*x(n) + 4*x(n-1)
    end
endfunction

function y = y2(x)
    for n = 4:length(x)+2
        y(n) = x(n-2) + 0.5*x(n-3)
    end
endfunction

normalniIzlaz = y2(y1(x));
inverzniIzlaz = y1(y2(x));

if (normalniIzlaz <> inverzniIzlaz) then
    printf("Ulazno-izlazna karakteristika sistema se menja.\n");
else    
    printf("Ulazno-izlazna karakteristika sistema ostaje ista.\n");
end


////////////////
//PROBLEM 1.16//
////////////////

printf("\nProblem 1.16:\n");

// a)

function y=signal(x)
    if (x<10) then
        y = 0
    else
        y = 1;
    end
endfunction

n = 10;
y = signal(n)*signal(n-2);

if (y == 0) then
    printf("Sistem je sa memorijom.\n");
else
    printf("Sistem je bez memorije.\n");
end

// b)δ[n]=1,n=0 i δ[n]=0,n!=0 => y[n]=0, za svako n
A=round(rand()*1000);
function y=f(x)
    if (x == 0) then
        y = 1*A;
    else
        y = 0;
    end
endfunction

kontrola = 0;
for n=-5000:5000
    y = f(n)*f(n-2);
    if (y <> 0) then
        kontrola = 1;
        break;
    end
end

if (kontrola == 1) then
    printf("Vrednost nije nula za svako n, -5000<=n<=5000.\n");
else
    printf("Vrednost je nula za svako n, -5000<=n<=5000.\n");
end

// c)
A=round(rand()*1000);
function y=f(x)
    if (x == 0) then
        y = 1*A;
    else
        y = 0;
    end
endfunction

kontrola = 1;
p=f(-5000)*f(5000-2);
for n=-4999:5000
    y = f(n)*f(n-2);
    if (y == p) then
        kontrola = 0;
        break;
    end
end

if (kontrola <> 1) then
    printf("Sistem nije invertibilan.\n");
else
    printf("Sistem je invertibilan.\n");
end


////////////////
//PROBLEM 1.17//
////////////////

printf("\nProblem 1.17:\n");

// a)
kontrola = 1;
pom = sin(-%pi);
for t = -2*%pi:%pi/8:2*%pi
    if (sin(t) == pom) then
        kontrola = 0;
        break;
    end
end

if (kontrola == 1) then
    printf("Sistem je kauzalan.\n");
else
    printf("Sistem nije kauzalan.\n");
end

// b)
x1 = [10 20 10 10];
x2 = [50 100 50 50];
x3 = [];
// Odredjivanje linearnosti metodom superpozicije
a1 = round(rand()*100);
a2 = round(rand()*100);

for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);    
end

y1 = [];
y2 = [];
y3 = [];
z = [];
i=1;
for t = 0:%pi/3:%pi
    y1(i) = x1(round(sin(t))+i);
    y2(i) = x2(round(sin(t))+i);
    y3(i) = x3(round(sin(t))+i);
    i=i+1;
end

for t = 1:length(y1)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan.\n")
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan.\n")    
end


////////////////
//PROBLEM 1.18//
////////////////

printf("\nProblem 1.18:\n");

// a)
function y = f(x, n, n0)
    y = 0;
    for t=n-n0:n+n0
        y = y + x(t);
    end
endfunction

x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [11 12 13 14 15 16 17 18 19 20];
x3 = [];
a1 = round(rand()*100);
a2 = round(rand()*100);

n0 = 2;

gornjaGranica = length(x1) - n0;
donjaGranica = n0+1;
for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);
end

y1 = [];
y2 = [];
y3 = [];
z = [];

for t = donjaGranica:gornjaGranica  
    y1(t-n0) = f(x1, t, n0);
    y2(t-n0) = f(x2, t, n0);
    y3(t-n0) = f(x3, t, n0);
end

for t = 1:length(y3)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan.\n");
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan.\n") ;   
end

// b)
function y = f(x, n, n0)
    y = 0;
    for t=n-n0:n+n0
        y = y + x(t);
    end
endfunction

n0 = 2;
n1 = 4;
x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [];
//x2 = [0 0 0 0 1 2 3 4 5 6 7 8 9 10];

for t =  n1+1 : length(x1)+n1
   x2(t)=x1(t-n1) 
end

y1=[];

gornjaGranica = length(x1)-n0;
donjaGranica = n0+1;
for t = donjaGranica:gornjaGranica
    y1(t-n0) = f(x1, t, n0);
end

y2=[];
gornjaGranica = length(x2)-n0;
donjaGranica = n0+1+n1;
for t = donjaGranica:gornjaGranica
    y2(t-n0) = f(x2, t, n0);
end

provera = 1;
for t = n1+1:length(y2)
    if (y1(t-n1) <> y2(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Sistem je vremenski invarijantan.\n")
else
    printf("Sistem nije vremenski invarijantan.\n")    
end

// c)
// |y[n]| ≤ Σ [od n-n0 do n+n0] |x[k]| i kako je |x[k]| <= B  sledi
// postoji konacan broj C <= B*(n+n0 - (n-n0) + 1) = B * (2*n0 + 1)

function y = f(x, n, n0)
    y = 0;
    for t=n-n0:n+n0
        y = y + x(t);
    end
endfunction


x = [0 1 2 3 4 5 6 7 8 9];
n0 = 2;
B = 10;
y = [];

gornjaGranica = length(x1)-n0;
donjaGranica = n0+1;
for t = donjaGranica:gornjaGranica
    y(t-n0) = f(x, t, n0);
end

provera = 1;
for t = 1:length(y)
    if (y(t)> (B*(2*n0+1))) then
        provera = 0;
        break;
    end;
end;

if (provera == 1) then
    printf("Kako vazi relacija: y[n] <= B*(2*n0+1), sledi da postoji konacan broj C <= B*(2*n0+1).\n");
else
    printf("Ne postoji konacan broj C za koji vazi C <= B*(2*n0+1)\n");
end


////////////////
//PROBLEM 1.19//
////////////////

printf("\nProblem 1.19:\n");

// a)
// linearnost

function y = f(x, t)
    y = t*t*x(t-1);
endfunction

x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [11 12 13 14 15 16 17 18 19 20];
x3 = [];
a1 = round(rand()*100);
a2 = round(rand()*100);

gornjaGranica = length(x1) + 1;
donjaGranica = 1+1;

for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);
end

y1 = [];
y2 = [];
y3 = [];
z = [];

for t = donjaGranica:gornjaGranica  
    y1(t-1) = f(x1, t);
    y2(t-1) = f(x2, t);
    y3(t-1) = f(x3, t);
end

for t = 1:length(y3)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan ");
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan ") ;   
end

// vremenska invarijantnost

function y = f(x, t)
    y = t*t*x(t-1);
endfunction

vremenskiPomeraj = 4;
x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [];
//x2 = [0 0 0 0 1 2 3 4 5 6 7 8 9 10];

for t =  vremenskiPomeraj+1 : length(x1)+vremenskiPomeraj
   x2(t)=x1(t-vremenskiPomeraj) 
end

y1=[];

gornjaGranica = length(x1) + 1;
donjaGranica = 1+1;
for t = donjaGranica:gornjaGranica
    y1(t-1) = f(x1, t);
end

y2=[];
gornjaGranica = length(x2)+1;
donjaGranica = 1+1+vremenskiPomeraj;
for t = donjaGranica:gornjaGranica
    y2(t-1) = f(x2, t);
end

provera = 1;
for t = vremenskiPomeraj+1:length(y2)
    if (y1(t-vremenskiPomeraj) <> y2(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("i vremenski  je invarijantan.\n")
else
    printf("i nije vremenski invarijantan.\n")    
end


// b)
// linearnost

function y = f(x, t)
    y = x(t-2)*x(t-2);
endfunction

x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [11 12 13 14 15 16 17 18 19 20];
x3 = [];
a1 = round(rand()*100);
a2 = round(rand()*100);

gornjaGranica = length(x1) + 2;
donjaGranica = 1+2;

for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);
end

y1 = [];
y2 = [];
y3 = [];
z = [];

for t = donjaGranica:gornjaGranica  
    y1(t-1) = f(x1, t);
    y2(t-1) = f(x2, t);
    y3(t-1) = f(x3, t);
end

for t = 1:length(y3)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan ");
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan ") ;   
end

// vremenska invarijantnost

function y = f(x, t)
    y = x(t-2)*x(t-2);
endfunction

vremenskiPomeraj = 4;
x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [];
//x2 = [0 0 0 0 1 2 3 4 5 6 7 8 9 10];

for t =  vremenskiPomeraj+1 : length(x1)+vremenskiPomeraj
   x2(t)=x1(t-vremenskiPomeraj) 
end

y1=[];

gornjaGranica = length(x1) + 2;
donjaGranica = 1+2;
for t = donjaGranica:gornjaGranica
    y1(t-2) = f(x1, t);
end

y2=[];
gornjaGranica = length(x2)+2;
donjaGranica = 1+2+vremenskiPomeraj;
for t = donjaGranica:gornjaGranica
    y2(t-2) = f(x2, t);
end

provera = 1;
for t = vremenskiPomeraj+1:length(y2)
    if (y1(t-vremenskiPomeraj) <> y2(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("i vremenski  je invarijantan.\n")
else
    printf("i nije vremenski invarijantan.\n")    
end

// c)
// linearnost

function y = f(x, t)
    y = x(t+1)-x(t-1);
endfunction

x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [11 12 13 14 15 16 17 18 19 20];
x3 = [];
a1 = round(rand()*100);
a2 = round(rand()*100);

gornjaGranica = length(x1) - 1;
donjaGranica = 1+1;

for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);
end

y1 = [];
y2 = [];
y3 = [];
z = [];

for t = donjaGranica:gornjaGranica  
    y1(t-1) = f(x1, t);
    y2(t-1) = f(x2, t);
    y3(t-1) = f(x3, t);
end

for t = 1:length(y3)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan ");
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan ") ;   
end

// vremenska invarijantnost

function y = f(x, t)
    y = x(t+1)-x(t-1);
endfunction

vremenskiPomeraj = 4;
x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [];
//x2 = [0 0 0 0 1 2 3 4 5 6 7 8 9 10];

for t =  vremenskiPomeraj+1 : length(x1)+vremenskiPomeraj
   x2(t)=x1(t-vremenskiPomeraj) 
end

y1=[];

gornjaGranica = length(x1) - 1;
donjaGranica = 1+1;
for t = donjaGranica:gornjaGranica
    y1(t-1) = f(x1, t);
end

y2=[];
gornjaGranica = length(x2)-1;
donjaGranica = 1+1+vremenskiPomeraj;
for t = donjaGranica:gornjaGranica
    y2(t-1) = f(x2, t);
end

provera = 1;
for t = vremenskiPomeraj+1:length(y2)
    if (y1(t-vremenskiPomeraj) <> y2(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("i vremenski  je invarijantan.\n")
else
    printf("i nije vremenski invarijantan.\n")    
end

// d)
// linearnost

x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [11 12 13 14 15 16 17 18 19 20];

function y = f(x, t)
    y = (x(t)-x(length(x)-t))/2;
endfunction



x3 = [];
a1 = round(rand()*100);
a2 = round(rand()*100);

gornjaGranica = length(x1)/2;
donjaGranica = 1;

for t = 1:length(x1)
    x3(t) = a1*x1(t) + a2*x2(t);
end

y1 = [];
y2 = [];
y3 = [];
z = [];

for t = donjaGranica:gornjaGranica  
    y1(t) = f(x1, t);
    y2(t) = f(x2, t);
    y3(t) = f(x3, t);
end

for t = 1:length(y3)
    z(t) = a1*y1(t)+a2*y2(t);
end

provera = 1;
for t = 1:length(y3)
    if (y3(t) <> z(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("Princip superpozicije je zadovoljen, sistem je linearan ");
else
    printf("Princip superpozicije nije zadovoljen, sistem nije linearan ") ;   
end

// vremenska invarijantnost

function y = f(x, t)
    y = (x(t)-x(length(x)-t))/2;
endfunction

vremenskiPomeraj = 4;
x1 = [1 2 3 4 5 6 7 8 9 10];
x2 = [];
//x2 = [0 0 0 0 1 2 3 4 5 6 7 8 9 10];

for t =  vremenskiPomeraj+1 : length(x1)+vremenskiPomeraj
   x2(t)=x1(t-vremenskiPomeraj) 
end

y1=[];

gornjaGranica = length(x1)/2;
donjaGranica = 1;
for t = donjaGranica:gornjaGranica
    y1(t) = f(x1, t);
end

y2=[];
gornjaGranica = length(x2)- length(x1)/2;
donjaGranica = 1+vremenskiPomeraj;
for t = donjaGranica:gornjaGranica
    y2(t) = f(x2, t);
end

provera = 1;
for t = vremenskiPomeraj+1:length(y2)
    if (y1(t-vremenskiPomeraj) <> y2(t))
        provera = 0;
        break;
    end
end

if (provera == 1) then
    printf("i vremenski  je invarijantan.\n")
else
    printf("i nije vremenski invarijantan.\n")    
end


////////////////
//PROBLEM 1.20//
////////////////

printf("\nProblem 1.20:\n");

// a)
    // x1(t)=exp(j*2*t), x2(t)=exp(-j*2*t)
    // y1(t)=exp(j*3*t), y2(t)=exp(-j*3*t)
    // a1=1/2, a2=1/2
    // Kako je u zadatku naglaseno da je sistem linearan, superpozicija vazi i
    // možemo oformiti linearnu kombinaciju dva ulazna signala x1 i x2
    // x3(t)=1/2*exp(j*2*t)+1/2exp(-j*2*t)=1/2(cos2t+j*sin2t+cos2t-j*sin2t)=cos2t
    // isto mozemo primeniti za izlazne signale i formiramo njihovu linearnu kombinaciju
    // y3(t)=1/2*exp(j*3*t)+1/2exp(-j*3*t)=1/2(cos3t+j*sin3t+cos3t-j*sin3t)=cos3t
    
    printf("Ako je ulazni signal x = cos(2t), tada je izlazni signal y = cos(3t).\n");

// b)
    // cos(2(t-1/2))=cos(2t-1)
    // x1(t)=exp(j*2*t), x2(t)=exp(-j*2*t)
    // y1(t)=exp(j*3*t), y2(t)=exp(-j*3*t)
    // a1=1/2*exp(-j), a2=1/2*exp(j)
    // Kako je u zadatku naglaseno da je sistem linearan, superpozicija vazi i
    // možemo oformiti linearnu kombinaciju dva ulazna signala x1 i x2
    // x3(t)=1/2*exp(j*2*t-j)+1/2exp(-j*2*t+j)=1/2(cos(2t-1)+j*sin(2t-1)+cos(2t-1)-j*sin(2t-1))=cos(2t-1)
    // isto mozemo primeniti za izlazne signale i formiramo njihovu linearnu kombinaciju
    // y3(t)=1/2*exp(j*3*t-j)+1/2exp(-j*3*t+j)=1/2(cos(3t-1)+j*sin(3t-1)+cos(3t-1)-j*sin(3t-1))=cos(3t-1)
    
    printf("Ako je ulazni signal x = cos(2t-1), tada je izlazni signal y = cos(3t-1).\n");
